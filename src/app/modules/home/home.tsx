import './home.scss';

import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import { connect } from 'react-redux';
import { Row, Col, CardImg } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';

interface IHomeProp extends StateProps, DispatchProps, RouteComponentProps {}

class Home extends React.Component<IHomeProp> {
  constructor(props) {
    super(props);
    this.handleGoToFirstSimulator = this.handleGoToFirstSimulator.bind(this);
    this.handleGoToSecondSimulator = this.handleGoToSecondSimulator.bind(this);
    this.handleGoToThirdSimulator = this.handleGoToThirdSimulator.bind(this);
  }

  handleGoToFirstSimulator() {
    this.props.history.push('/entity/simulator/lmnp/old');
  }

  handleGoToSecondSimulator() {
    this.props.history.push('/entity/simulator/lmnp/old');
  }

  handleGoToThirdSimulator() {
    this.props.history.push('/entity/simulator/lmnp/old');
  }

  render = () => (
    <>
      <Row>
        <Col md="12" className="text-center">
          <h2>
            <Translate contentKey="home.title" />
          </h2>
          <p className="lead">
            <Translate contentKey="home.subtitle">This is your homepage</Translate>
          </p>
        </Col>
        <Col md="4">
          <CardImg
            className="btn"
            onClick={this.handleGoToSecondSimulator}
            top
            width="100%"
            src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
            alt="Card image cap"
          />
        </Col>
        <Col md="4">
          <CardImg
            className="btn"
            onClick={this.handleGoToSecondSimulator}
            top
            width="100%"
            src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
            alt="Card image cap"
          />
        </Col>
        <Col md="4">
          <CardImg
            className="btn"
            onClick={this.handleGoToThirdSimulator}
            top
            width="100%"
            src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180"
            alt="Card image cap"
          />
        </Col>
      </Row>
    </>
  );
}

const mapStateToProps = (state: IRootState) => ({});

const mapDispatchToProps = {};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
