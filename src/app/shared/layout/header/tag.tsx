import React from 'react';
import { UncontrolledTooltip, Badge } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

interface ITagProp {
  title: string;
  color: string;
  value: number;
  symbol: string;
  tooltipText: string;
  tooltipId: string;
}

export class Tag extends React.Component<ITagProp> {
  constructor(props) {
    super(props);
  }

  render = () => (
    <Badge color={this.props.color} className="py-2 ml-auto">
      {this.props.value}
      {this.props.symbol}
      <span id={this.props.tooltipId}>
        {' '}
        <FontAwesomeIcon icon={faInfoCircle} />
      </span>
      <UncontrolledTooltip target={this.props.tooltipId}>{this.props.tooltipText}</UncontrolledTooltip>
    </Badge>
  );
}
