import './header.scss';

import React from 'react';
import { Translate, Storage } from 'react-jhipster';
import { Navbar, Nav, NavbarToggler, Collapse } from 'reactstrap';
import LoadingBar from 'react-redux-loading-bar';
import { Home, Brand } from './header-components';
import { AdminMenu, AccountMenu, LocaleMenu } from './menus';
import { Tag } from './tag';

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  currentLocale: string;
  onLocaleChange: Function;
  gross: number;
  net: number;
}

export interface IHeaderState {
  menuOpen: boolean;
}

export default class Header extends React.Component<IHeaderProps, IHeaderState> {
  state: IHeaderState = {
    menuOpen: false
  };

  handleGrossColor() {
    const gross = this.props.gross;
    let result = 'info';
    if (gross >= 9) {
      result = 'success';
    } else if (gross >= 7 && gross < 9) {
      result = 'warning';
    } else if (gross > 0 && gross < 7) {
      result = 'danger';
    }
    return result;
  }

  handleNetColor() {
    const net = this.props.net;
    let result = 'info';
    if (net >= 7) {
      result = 'success';
    } else if (net >= 5 && net < 7) {
      result = 'warning';
    } else if (net > 0 && net < 5) {
      result = 'danger';
    }
    return result;
  }

  handleLocaleChange = event => {
    const langKey = event.target.value;
    Storage.session.set('locale', langKey);
    this.props.onLocaleChange(langKey);
  };

  renderDevRibbon = () =>
    this.props.isInProduction === false ? (
      <div className="ribbon dev">
        <a href="">
          <Translate contentKey={`global.ribbon.${this.props.ribbonEnv}`} />
        </a>
      </div>
    ) : null;

  toggleMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
  };

  render() {
    const { currentLocale, isAuthenticated, isAdmin, isInProduction } = this.props;

    /* jhipster-needle-add-element-to-menu - JHipster will add new menu items here */

    return (
      <div id="app-header">
        {this.renderDevRibbon()}
        <LoadingBar className="loading-bar" />
        <Navbar dark expand="lg" fixed="top" className="jh-navbar">
          <NavbarToggler aria-label="Menu" onClick={this.toggleMenu} />
          <Brand />
          <Tag
            title="BRUT"
            color={this.handleGrossColor()}
            value={this.props.gross}
            symbol="%"
            tooltipId="tooltip-id-1"
            tooltipText="Rendement locatif BRUT"
          />
          <Tag
            title="NET"
            color={this.handleNetColor()}
            value={this.props.net}
            symbol="%"
            tooltipId="tooltip-id-2"
            tooltipText="Rendement locatif NET lors de la première année"
          />
          <Tag
            title="NET NET"
            color="info"
            value={10.01}
            symbol="%"
            tooltipId="tooltip-id-3"
            tooltipText="Rendement locatif NET après impôts lors de la première année"
          />
          <Tag
            title="CASHFLOW"
            color="success"
            value={1000.01}
            symbol="€"
            tooltipId="tooltip-id-4"
            tooltipText="Cashflow mensuel à la fin de la première année"
          />
          <Collapse isOpen={this.state.menuOpen} navbar>
            <Nav id="header-tabs" className="ml-auto" navbar>
              <Home />
              {/*isAuthenticated && <EntitiesMenu />*/}
              {isAuthenticated && isAdmin && <AdminMenu />}
              <LocaleMenu currentLocale={currentLocale} onClick={this.handleLocaleChange} />
              <AccountMenu isAuthenticated={isAuthenticated} />
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
