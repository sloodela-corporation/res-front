import { SUCCESS } from 'app/shared/util/action.type.util';
import { ACTION_TYPES } from '../actions/application.profile.action';

const initialState = {
  ribbonEnv: '',
  inProduction: true
};

export type ApplicationProfileState = Readonly<typeof initialState>;

export default (state: ApplicationProfileState = initialState, action): ApplicationProfileState => {
  switch (action.type) {
    case SUCCESS(ACTION_TYPES.GET_PROFILE):
      const { data } = action.payload;
      return {
        ...state,
        ribbonEnv: data['display-ribbon-on-profiles'],
        inProduction: data.activeProfiles.includes('prod')
      };
    default:
      return state;
  }
};
