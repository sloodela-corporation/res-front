import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale.reducer';
import authentication, { AuthenticationState } from './authentication.reducer';
import applicationProfile, { ApplicationProfileState } from './application.profile.reducer';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
import lmnpOldSettings from 'app/entities/simulators/lmnp/old/settings/settings.reducer';
import { ISettingsModel as LmnpOldSettingsState } from 'app/entities/simulators/lmnp/old/settings/settings.model';
import lmnpOldSeller from 'app/entities/simulators/lmnp/old/seller/seller.reducer';
import { ISellerModel as LmnpSellerState } from 'app/entities/simulators/lmnp/old/seller/seller.model';
import lmnpOldBuyer from 'app/entities/simulators/lmnp/old/buyer/buyer.reducer';
import { IBuyerModel as LmnpBuyerState } from 'app/entities/simulators/lmnp/old/buyer/buyer.model';
import lmnpOld from 'app/entities/simulators/lmnp/old/lmnp.old.reducer';
import { ILmnpModel as LmnpState } from 'app/entities/simulators/lmnp/old/lmnp.old.model';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly lmnpOldSettings: LmnpOldSettingsState;
  readonly lmnpOldSeller: LmnpSellerState;
  readonly lmnpOldBuyer: LmnpBuyerState;
  readonly lmnpOld: LmnpState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  lmnpOldSettings,
  lmnpOldBuyer,
  lmnpOldSeller,
  lmnpOld,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
