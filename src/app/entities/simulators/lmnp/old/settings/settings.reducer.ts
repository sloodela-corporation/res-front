import { ISettingsModel } from './settings.model';

const initialState: ISettingsModel = {
  notaryRate: 0.07,
  agencyRate: 0.07,
  rentalManagementRate: 0.09,
  bankGuaranteeOptions: [{ name: 'Caution', rate: 0.0075 }, { name: 'Hypothèque', rate: 0.015 }],
  certifiedManagementCenterLimit: 900,
  certifiedManagementCenterReductionRate: 0.66,
  socialSecurityDeductionRate: 0.172,
  microBicAbatementRate: 0.5,
  fieldNoAmortisableRate: 0.15,
  amortisations: [
    { group: 'bien', name: 'gros oeuvre', duration: 50, rate: 0.34 },
    { group: 'bien', name: 'façade', duration: 30, rate: 0.17 },
    { group: 'bien', name: 'equipement', duration: 20, rate: 0.17 },
    { group: 'bien', name: 'agencement', duration: 15, rate: 0.17 },
    { group: 'meubles', name: 'meubles', duration: 7, rate: 1 },
    { group: 'travaux', name: 'travaux', duration: 10, rate: 1 }
  ]
};

export default (state: Readonly<ISettingsModel> = initialState, action): Readonly<ISettingsModel> => {
  switch (action.type) {
    default:
      return state;
  }
};
