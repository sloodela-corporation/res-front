export interface ISettingsModel {
  notaryRate: number; // Pourcentage des frais de notaire
  agencyRate: number; // Pourcentage des frais d'agence
  rentalManagementRate: number; // Pourcentage de la gestion locative
  bankGuaranteeOptions: IBankGuaranteeOptionsModel[]; // Types de garantie bancaire
  certifiedManagementCenterLimit: number; // Plafond Centre de gestion agréé
  certifiedManagementCenterReductionRate: number; // Pourcentage de réduction pour les CGA
  socialSecurityDeductionRate: number; // CSG-CRDS
  microBicAbatementRate: number; // Pourcentage d'abattement pour la fiscalité micro-BIC
  fieldNoAmortisableRate: number; // Pourcentage du terrain non-amortissable
  amortisations: IAmortisationModel[]; // Amortissements
}

interface IBankGuaranteeOptionsModel {
  name: string; // Nom de la garantie
  rate: number; // taux de la garantie
}

interface IAmortisationModel {
  group: string; // Famille d'amortissement
  name: string; // Nom de la garantie
  duration: number; // Durée de l'amortissement
  rate: number; // Pourcentage de la part à amortir
}
