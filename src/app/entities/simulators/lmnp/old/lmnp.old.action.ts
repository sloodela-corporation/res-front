import { processGrossReturn, processNetReturn } from './lmnp.old.service';

export const ACTION_TYPES = {
  ON_CHANGE_TAB: 'lmnp/ON_CHANGE_TAB',
  PROCESS_GROSS: 'lmnp/PROCESS_GROSS',
  PROCESS_NET: 'lmnp/PROCESS_NET'
};

export const changeTabAction = newTabId => ({
  type: ACTION_TYPES.ON_CHANGE_TAB,
  payload: newTabId
});

export const processGrossAction = form => ({
  type: ACTION_TYPES.PROCESS_GROSS,
  payload: processGrossReturn(form)
});

export const processNetAction = form => ({
  type: ACTION_TYPES.PROCESS_NET,
  payload: processNetReturn()
});
