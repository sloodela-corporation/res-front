import { ACTION_TYPES } from './lmnp.old.action';
import { ILmnpModel } from './lmnp.old.model';

const initialState: ILmnpModel = {
  tab: { active: '1' },
  gross: 0,
  net: 0
};

export default (state: Readonly<ILmnpModel> = initialState, action): Readonly<ILmnpModel> => {
  switch (action.type) {
    case ACTION_TYPES.ON_CHANGE_TAB:
      return {
        ...state,
        tab: { ...state.tab, active: action.payload }
      };
    case ACTION_TYPES.PROCESS_GROSS:
      return {
        ...state,
        gross: action.payload
      };
    case ACTION_TYPES.PROCESS_NET:
      return {
        ...state,
        net: action.payload
      };
    default:
      return state;
  }
};
