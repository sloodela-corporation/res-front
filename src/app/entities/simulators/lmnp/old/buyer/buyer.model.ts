export interface IBuyerModel {
  loanRate: number;
  loanDuration: number;
  insuranceRate: number;
  applicationFee: number;
  bankGuarantee: string;
  insurancePrice: number;
  isRentalManagement: boolean;
  rentalManagement: number;
  payAsYouEarn: number;
  activityDate: string;
  accountantPrice: number;
  isCertifiedManagementCenter: boolean;
  certifiedManagementCenter: number;
}
