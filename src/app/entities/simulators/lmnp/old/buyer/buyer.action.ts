import { processNetAction } from '../lmnp.old.action';

export const ACTION_TYPES = {
  ON_CHANGE_LOAN_RATE: 'buyer/ON_CHANGE_LOAN_RATE',
  ON_CHANGE_LOAN_DURATION: 'buyer/ON_CHANGE_LOAN_DURATION',
  ON_CHANGE_INSURANCE_RATE: 'buyer/ON_CHANGE_INSURANCE_RATE',
  ON_CHANGE_APPLICATION_FEE: 'buyer/ON_CHANGE_APPLICATION_FEE',
  ON_CHANGE_BANK_GUARANTEE: 'buyer/ON_CHANGE_BANK_GUARANTEE',
  ON_CHANGE_INSURANCE_PRICE: 'buyer/ON_CHANGE_INSURANCE_PRICE',
  ON_CHANGE_IS_RENTAL_MANAGEMENT: 'buyer/ON_CHANGE_IS_RENTAL_MANAGEMENT',
  ON_CHANGE_RENTAL_MANAGEMENT: 'buyer/ON_CHANGE_RENTAL_MANAGEMENT',
  ON_CHANGE_PAY_AS_YOU_EARN: 'buyer/ON_CHANGE_PAY_AS_YOU_EARN',
  ON_CHANGE_ACTIVITY_DATE: 'buyer/ON_CHANGE_ACTIVITY_DATE',
  ON_CHANGE_ACCOUNTANT_PRICE: 'buyer/ON_CHANGE_ACCOUNTANT_PRICE',
  ON_CHANGE_IS_CERTIFIED_MANAGEMENT_CENTER: 'buyer/ON_CHANGE_IS_CERTIFIED_MANAGEMENT_CENTER',
  ON_CHANGE_CERTIFIED_MANAGEMENT_CENTER: 'buyer/ON_CHANGE_CERTIFIED_MANAGEMENT_CENTER'
};

export const onChangeLoanRateAction = (buyerForm, loanRate) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_LOAN_RATE,
    payload: { newValue: loanRate }
  });
  const newForm = { ...buyerForm, loanRate };
  await dispatch(processNetAction(newForm));
};

export const onChangeLoanDurationAction = (buyerForm, loanDuration) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_LOAN_DURATION,
    payload: { newValue: loanDuration }
  });
  const newForm = { ...buyerForm, loanDuration };
  await dispatch(processNetAction(newForm));
};

export const onChangeInsuranceRateAction = (buyerForm, insuranceRate) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_INSURANCE_RATE,
    payload: { newValue: insuranceRate }
  });
  const newForm = { ...buyerForm, insuranceRate };
  await dispatch(processNetAction(newForm));
};

export const onChangeApplicationFeeAction = (buyerForm, applicationFee) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_APPLICATION_FEE,
    payload: { newValue: applicationFee }
  });
  const newForm = { ...buyerForm, applicationFee };
  await dispatch(processNetAction(newForm));
};

export const onChangeBankGuaranteeAction = (buyerForm, bankGuarantee) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_BANK_GUARANTEE,
    payload: { newValue: bankGuarantee }
  });
  const newForm = { ...buyerForm, bankGuarantee };
  await dispatch(processNetAction(newForm));
};

export const onChangeInsurancePriceAction = (buyerForm, insurancePrice) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_INSURANCE_PRICE,
    payload: { newValue: insurancePrice }
  });
  const newForm = { ...buyerForm, insurancePrice };
  await dispatch(processNetAction(newForm));
};

export const onChangeIsRentalManagementAction = (buyerForm, b, isRentalManagement) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_IS_RENTAL_MANAGEMENT,
    payload: { newValue: isRentalManagement }
  });
  const newForm = { ...buyerForm, isRentalManagement };
  await dispatch(processNetAction(newForm));
};

export const onChangeRentalManagementAction = (buyerForm, rentalManagement) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_RENTAL_MANAGEMENT,
    payload: { newValue: rentalManagement }
  });
  const newForm = { ...buyerForm, rentalManagement };
  await dispatch(processNetAction(newForm));
};

export const onChangePayAsYouEarnAction = (buyerForm, payAsYouEarn) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_PAY_AS_YOU_EARN,
    payload: { newValue: payAsYouEarn }
  });
  const newForm = { ...buyerForm, payAsYouEarn };
  await dispatch(processNetAction(newForm));
};

export const onChangeActivityDateAction = (buyerForm, activityDate) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_ACTIVITY_DATE,
    payload: { newValue: activityDate }
  });
  const newForm = { ...buyerForm, activityDate };
  await dispatch(processNetAction(newForm));
};

export const onChangeAccountantPriceAction = (buyerForm, accountantPrice) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_ACCOUNTANT_PRICE,
    payload: { newValue: accountantPrice }
  });
  const newForm = { ...buyerForm, accountantPrice };
  await dispatch(processNetAction(newForm));
};

export const onChangeCertifiedManagementCenterAction = (buyerForm, certifiedManagementCenter) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_CERTIFIED_MANAGEMENT_CENTER,
    payload: { newValue: certifiedManagementCenter }
  });
  const newForm = { ...buyerForm, certifiedManagementCenter };
  await dispatch(processNetAction(newForm));
};

export const onChangeIsCertifiedManagementCenterAction = (buyerForm, isCertifiedManagementCenter) => ({
  type: ACTION_TYPES.ON_CHANGE_IS_CERTIFIED_MANAGEMENT_CENTER,
  payload: { newValue: isCertifiedManagementCenter }
});
