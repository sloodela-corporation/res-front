import React from 'react';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Row, Col, UncontrolledTooltip, Input, Form, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import {
  onChangeLoanDurationAction,
  onChangeLoanRateAction,
  onChangeInsuranceRateAction,
  onChangeApplicationFeeAction,
  onChangeBankGuaranteeAction,
  onChangeInsurancePriceAction,
  onChangeIsRentalManagementAction,
  onChangeRentalManagementAction,
  onChangePayAsYouEarnAction,
  onChangeActivityDateAction,
  onChangeAccountantPriceAction,
  onChangeCertifiedManagementCenterAction,
  onChangeIsCertifiedManagementCenterAction
} from './buyer.action';

interface IBuyerFormProps extends DispatchProps, StateProps {}

class BuyerForm extends React.Component<IBuyerFormProps> {
  constructor(props) {
    super(props);
    this.handleOnChangeLoanRate = this.handleOnChangeLoanRate.bind(this);
    this.handleOnChangeLoanDuration = this.handleOnChangeLoanDuration.bind(this);
    this.handleOnChangeInsuranceRate = this.handleOnChangeInsuranceRate.bind(this);
    this.handleOnChangeApplicationFee = this.handleOnChangeApplicationFee.bind(this);
    this.handleOnChangeBankGuarantee = this.handleOnChangeBankGuarantee.bind(this);
    this.handleOnChangeInsurancePrice = this.handleOnChangeInsurancePrice.bind(this);
    this.handleOnChangeIsRentalManagement = this.handleOnChangeIsRentalManagement.bind(this);
    this.handleOnChangeRentalManagement = this.handleOnChangeRentalManagement.bind(this);
    this.handleOnChangePayAsYouEarn = this.handleOnChangePayAsYouEarn.bind(this);
    this.handleOnChangeActivityDate = this.handleOnChangeActivityDate.bind(this);
    this.handleOnChangeAccountantPrice = this.handleOnChangeAccountantPrice.bind(this);
    this.handleOnChangeIsCertifiedManagementCenter = this.handleOnChangeIsCertifiedManagementCenter.bind(this);
    this.handleOnChangeCertifiedManagementCenter = this.handleOnChangeCertifiedManagementCenter.bind(this);
  }

  handleOnChangeLoanRate(e) {
    if (e.target.value) {
      this.props.onChangeLoanRateAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeLoanRateAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeLoanDuration(e) {
    if (e.target.value) {
      this.props.onChangeLoanDurationAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeLoanDurationAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeInsuranceRate(e) {
    if (e.target.value) {
      this.props.onChangeInsuranceRateAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeInsuranceRateAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeApplicationFee(e) {
    if (e.target.value) {
      this.props.onChangeApplicationFeeAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeApplicationFeeAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeBankGuarantee(e) {
    if (e.target.value) {
      this.props.onChangeBankGuaranteeAction(this.props.lmnpOldBuyer, parseFloat(e.target.value));
    } else {
      this.props.onChangeBankGuaranteeAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeInsurancePrice(e) {
    if (e.target.value) {
      this.props.onChangeInsurancePriceAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeInsurancePriceAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeIsRentalManagement(e) {
    this.props.onChangeIsRentalManagementAction(this.props.lmnpOldSettings.rentalManagementRate, this.props.lmnpOldBuyer, e.target.checked);
  }

  handleOnChangeRentalManagement(e) {
    if (e.target.value) {
      this.props.onChangeRentalManagementAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeRentalManagementAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangePayAsYouEarn(e) {
    if (e.target.value) {
      this.props.onChangePayAsYouEarnAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangePayAsYouEarnAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeActivityDate(e) {
    this.props.onChangeActivityDateAction(this.props.lmnpOldBuyer, e.target.value);
  }

  handleOnChangeAccountantPrice(e) {
    if (e.target.value) {
      this.props.onChangeAccountantPriceAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeAccountantPriceAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeCertifiedManagementCenter(e) {
    if (e.target.value) {
      this.props.onChangeCertifiedManagementCenterAction(this.props.lmnpOldBuyer, e.target.valueAsNumber);
    } else {
      this.props.onChangeCertifiedManagementCenterAction(this.props.lmnpOldBuyer, 0);
    }
  }

  handleOnChangeIsCertifiedManagementCenter(e) {
    this.props.onChangeIsCertifiedManagementCenterAction(this.props.lmnpOldBuyer, e.target.checked);
  }

  render = () => (
    <Form>
      <Row>
        <Col md="6" xs="12">
          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.buyer.form.legend.bank" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.loanrate.placeholder')}
                onChange={this.handleOnChangeLoanRate}
                value={this.props.lmnpOldBuyer.loanRate || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-loan-rate">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-loan-rate">
                  <Translate contentKey="lmnp.old.buyer.form.loanrate.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.loanduration.placeholder')}
                onChange={this.handleOnChangeLoanDuration}
                value={this.props.lmnpOldBuyer.loanDuration || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-loan-duration">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-loan-duration">
                  <Translate contentKey="lmnp.old.buyer.form.loanduration.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.insurancerate.placeholder')}
                onChange={this.handleOnChangeInsuranceRate}
                value={this.props.lmnpOldBuyer.insuranceRate || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-insurance-rate">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-insurance-rate">
                  <Translate contentKey="lmnp.old.buyer.form.insurancerate.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.applicationfee.placeholder')}
                onChange={this.handleOnChangeApplicationFee}
                value={this.props.lmnpOldBuyer.applicationFee || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-application-fee">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-application-fee">
                  <Translate contentKey="lmnp.old.buyer.form.applicationfee.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <InputGroupAddon addonType="prepend">
                <Input id="bank-guarantee" name="bank-guarantee" bsSize="sm" type="select" onChange={this.handleOnChangeBankGuarantee}>
                  {this.props.lmnpOldSettings.bankGuaranteeOptions.map((e, key) => (
                    <option key={key} value={e.rate}>
                      {e.name}
                    </option>
                  ))}
                </Input>
              </InputGroupAddon>
              <Input
                placeholder={translate('lmnp.old.buyer.form.bankguarantee.placeholder')}
                value={this.props.lmnpOldBuyer.bankGuarantee || this.props.lmnpOldSettings.bankGuaranteeOptions[0].rate}
                type="number"
                disabled
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-bank-guarantee">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-bank-guarantee">
                  <Translate contentKey="lmnp.old.buyer.form.bankguarantee.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>

          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.buyer.form.legend.insurance" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.insuranceprice.placeholder')}
                onChange={this.handleOnChangeInsurancePrice}
                value={this.props.lmnpOldBuyer.insurancePrice || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-insurance-price">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-insurance-price">
                  <Translate contentKey="lmnp.old.buyer.form.insuranceprice.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>
        </Col>
        <Col md="6" xs="12">
          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.buyer.form.legend.fees" />
              </small>
            </legend>

            <InputGroup size="sm">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <Input addon onChange={this.handleOnChangeIsRentalManagement} type="checkbox" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder={translate('lmnp.old.buyer.form.rentalmanagement.placeholder')}
                onChange={this.handleOnChangeRentalManagement}
                value={this.props.lmnpOldBuyer.rentalManagement || ''}
                disabled={this.props.lmnpOldBuyer.isRentalManagement}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-rental-management">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-rental-management">
                  <Translate contentKey="lmnp.old.buyer.form.rentalmanagement.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>

          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.buyer.form.legend.taxation" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.paye.placeholder')}
                onChange={this.handleOnChangePayAsYouEarn}
                value={this.props.lmnpOldBuyer.payAsYouEarn || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-pay-as-you-earn">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-pay-as-you-earn">
                  <Translate contentKey="lmnp.old.buyer.form.paye.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input onChange={this.handleOnChangeActivityDate} value={this.props.lmnpOldBuyer.activityDate} type="date" />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-activity-date">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-activity-date">
                  <Translate contentKey="lmnp.old.buyer.form.activitydate.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.buyer.form.accountantprice.placeholder')}
                onChange={this.handleOnChangeAccountantPrice}
                value={this.props.lmnpOldBuyer.accountantPrice || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-accountant-price">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-accountant-price">
                  <Translate contentKey="lmnp.old.buyer.form.accountantprice.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <Input addon onChange={this.handleOnChangeIsCertifiedManagementCenter} type="checkbox" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder={translate('lmnp.old.buyer.form.certifiedmanagementcenter.placeholder')}
                onChange={this.handleOnChangeCertifiedManagementCenter}
                value={this.props.lmnpOldBuyer.certifiedManagementCenter || ''}
                disabled={!this.props.lmnpOldBuyer.isCertifiedManagementCenter}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-certified-management-center">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-certified-management-center">
                  <Translate contentKey="lmnp.old.buyer.form.certifiedmanagementcenter.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>
        </Col>
      </Row>
    </Form>
  );
}

const mapStateToProps = (state: IRootState) => ({
  lmnpOldBuyer: state.lmnpOldBuyer,
  lmnpOldSeller: state.lmnpOldSeller,
  lmnpOldSettings: state.lmnpOldSettings
});

const mapDispatchToProps = {
  onChangeLoanDurationAction,
  onChangeLoanRateAction,
  onChangeInsuranceRateAction,
  onChangeApplicationFeeAction,
  onChangeBankGuaranteeAction,
  onChangeInsurancePriceAction,
  onChangeIsRentalManagementAction,
  onChangeRentalManagementAction,
  onChangePayAsYouEarnAction,
  onChangeActivityDateAction,
  onChangeAccountantPriceAction,
  onChangeCertifiedManagementCenterAction,
  onChangeIsCertifiedManagementCenterAction
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuyerForm);
