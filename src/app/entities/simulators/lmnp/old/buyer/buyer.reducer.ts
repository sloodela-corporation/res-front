import { ACTION_TYPES } from './buyer.action';
import { IBuyerModel } from './buyer.model';

const initialState: IBuyerModel = {
  loanRate: 0,
  loanDuration: 0,
  insuranceRate: 0,
  applicationFee: 0,
  bankGuarantee: '',
  insurancePrice: 0,
  isRentalManagement: false,
  rentalManagement: 0,
  payAsYouEarn: 0,
  activityDate: new Date().toISOString().substr(0, 10),
  accountantPrice: 0,
  isCertifiedManagementCenter: false,
  certifiedManagementCenter: 0
};

export default (state: Readonly<IBuyerModel> = initialState, action): Readonly<IBuyerModel> => {
  switch (action.type) {
    case ACTION_TYPES.ON_CHANGE_LOAN_RATE:
      return {
        ...state,
        loanRate: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_LOAN_DURATION:
      return {
        ...state,
        loanDuration: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_INSURANCE_RATE:
      return {
        ...state,
        insuranceRate: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_APPLICATION_FEE:
      return {
        ...state,
        applicationFee: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_BANK_GUARANTEE:
      return {
        ...state,
        bankGuarantee: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_INSURANCE_PRICE:
      return {
        ...state,
        insurancePrice: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_IS_RENTAL_MANAGEMENT:
      return {
        ...state,
        isRentalManagement: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_RENTAL_MANAGEMENT:
      return {
        ...state,
        rentalManagement: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_ACTIVITY_DATE:
      return {
        ...state,
        activityDate: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_ACCOUNTANT_PRICE:
      return {
        ...state,
        accountantPrice: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_IS_CERTIFIED_MANAGEMENT_CENTER:
      return {
        ...state,
        isCertifiedManagementCenter: action.payload.newValue,
        certifiedManagementCenter: action.payload.newValue ? state.certifiedManagementCenter : 0
      };
    case ACTION_TYPES.ON_CHANGE_CERTIFIED_MANAGEMENT_CENTER:
      return {
        ...state,
        certifiedManagementCenter: action.payload.newValue
      };
    default:
      return state;
  }
};
