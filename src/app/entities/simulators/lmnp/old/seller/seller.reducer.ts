import { ACTION_TYPES } from './seller.action';
import { ISellerModel } from './seller.model';

const initialState: ISellerModel = {
  price: 0,
  notary: 0,
  agency: 0,
  works: 0,
  furnitures: 0,
  otherExpenses: 0,
  contribution: 0,
  rent: 0,
  charges: 0,
  vacancy: 0,
  otherIncomes: 0,
  propertyTax: 0,
  condominiumFees: 0,
  isNotaryAutomaticCalculation: false,
  isAgencyAutomaticCalculation: false
};

export default (state: Readonly<ISellerModel> = initialState, action): Readonly<ISellerModel> => {
  switch (action.type) {
    case ACTION_TYPES.ON_CHANGE_PRICE:
      return {
        ...state,
        price: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_NOTARY:
      return {
        ...state,
        notary: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_AGENCY:
      return {
        ...state,
        agency: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_WORKS:
      return {
        ...state,
        works: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_FURNITURES:
      return {
        ...state,
        furnitures: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_OTHER_EXPENSES:
      return {
        ...state,
        otherExpenses: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_CONTRIBUTION:
      return {
        ...state,
        contribution: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_RENT:
      return {
        ...state,
        rent: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_CHARGES:
      return {
        ...state,
        charges: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_VACANCY:
      return {
        ...state,
        vacancy: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_OTHER_INCOMES:
      return {
        ...state,
        otherIncomes: action.payload.newValue
      };
    case ACTION_TYPES.ON_CHANGE_IS_NOTARY_AUTOMATIC_CALCULATION:
      return {
        ...state,
        notary: action.payload.isNeedToProceed ? action.payload.newValue : state.notary,
        isNotaryAutomaticCalculation: action.payload.isNeedToProceed
      };
    case ACTION_TYPES.ON_CHANGE_IS_AGENCY_AUTOMATIC_CALCULATION:
      return {
        ...state,
        agency: action.payload.isNeedToProceed ? action.payload.newValue : state.agency,
        isAgencyAutomaticCalculation: action.payload.isNeedToProceed
      };
    case ACTION_TYPES.ON_CHANGE_PROPERTY_TAX:
      return {
        ...state,
        propertyTax: action.payload.propertyTax
      };
    case ACTION_TYPES.ON_CHANGE_CONDOMINIUM_FEES:
      return {
        ...state,
        condominiumFees: action.payload.condominiumFees
      };
    default:
      return state;
  }
};
