import { processCalculation } from './seller.service';
import { processGrossAction, processNetAction } from '../lmnp.old.action';

export const ACTION_TYPES = {
  ON_CHANGE_PRICE: 'seller/ON_CHANGE_PRICE',
  ON_CHANGE_NOTARY: 'seller/ON_CHANGE_NOTARY',
  ON_CHANGE_AGENCY: 'seller/ON_CHANGE_AGENCY',
  ON_CHANGE_WORKS: 'seller/ON_CHANGE_WORKS',
  ON_CHANGE_FURNITURES: 'seller/ON_CHANGE_FURNITURES',
  ON_CHANGE_OTHER_EXPENSES: 'seller/ON_CHANGE_OTHER_EXPENSES',
  ON_CHANGE_CONTRIBUTION: 'seller/ON_CHANGE_CONTRIBUTION',
  ON_CHANGE_RENT: 'seller/ON_CHANGE_RENT',
  ON_CHANGE_CHARGES: 'seller/ON_CHANGE_CHARGES',
  ON_CHANGE_VACANCY: 'seller/ON_CHANGE_VACANCY',
  ON_CHANGE_OTHER_INCOMES: 'seller/ON_CHANGE_OTHER_INCOMES',
  ON_CHANGE_PROPERTY_TAX: 'buyer/ON_CHANGE_PROPERTY_TAX',
  ON_CHANGE_CONDOMINIUM_FEES: 'buyer/ON_CHANGE_CONDOMINIUM_FEES',
  ON_CHANGE_IS_NOTARY_AUTOMATIC_CALCULATION: 'seller/ON_CHANGE_IS_NOTARY_AUTOMATIC_CALCULATION',
  ON_CHANGE_IS_AGENCY_AUTOMATIC_CALCULATION: 'seller/ON_CHANGE_IS_AGENCY_AUTOMATIC_CALCULATION'
};

export const processAutomaticPriceAgency = (agencyPercent, form, isNeedToProceed) => async dispatch => {
  const newValue = processCalculation(agencyPercent, form.price, isNeedToProceed);
  const agency = isNeedToProceed ? newValue : form.agency;
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_IS_AGENCY_AUTOMATIC_CALCULATION,
    payload: {
      newValue: agency,
      isNeedToProceed
    }
  });
  const newForm = { ...form, agency };
  await dispatch(processGrossAction(newForm));
};

export const processAutomaticPriceNotary = (notaryPercent, form, isNeedToProceed) => async dispatch => {
  const newValue = processCalculation(notaryPercent, form.price, isNeedToProceed);
  const notary = isNeedToProceed ? newValue : form.notary;
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_IS_NOTARY_AUTOMATIC_CALCULATION,
    payload: {
      newValue: isNeedToProceed ? newValue : form.notary,
      isNeedToProceed
    }
  });
  const newForm = { ...form, notary };
  await dispatch(processGrossAction(newForm));
};

export const onChangePriceAction = (form, price) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_PRICE,
    payload: { newValue: price }
  });
  const newForm = { ...form, price };
  await dispatch(processGrossAction(newForm));
};

export const onChangeNotaryAction = (form, notary) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_NOTARY,
    payload: { newValue: notary }
  });
  const newForm = { ...form, notary };
  await dispatch(processGrossAction(newForm));
};

export const onChangeAgencyAction = (form, agency) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_AGENCY,
    payload: { newValue: agency }
  });
  const newForm = { ...form, agency };
  await dispatch(processGrossAction(newForm));
};

export const onChangeWorksAction = (form, works) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_WORKS,
    payload: { newValue: works }
  });
  const newForm = { ...form, works };
  await dispatch(processGrossAction(newForm));
};

export const onChangeFurnituresAction = (form, furnitures) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_FURNITURES,
    payload: { newValue: furnitures }
  });
  const newForm = { ...form, furnitures };
  await dispatch(processGrossAction(newForm));
};

export const onChangeOtherExpensesAction = (form, otherExpenses) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_OTHER_EXPENSES,
    payload: { newValue: otherExpenses }
  });
  const newForm = { ...form, otherExpenses };
  await dispatch(processGrossAction(newForm));
};

export const onChangeContributionAction = (form, contribution) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_CONTRIBUTION,
    payload: { newValue: contribution }
  });
  const newForm = { ...form, contribution };
  await dispatch(processGrossAction(newForm));
};

export const onChangeRentAction = (form, rent) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_RENT,
    payload: { newValue: rent }
  });
  const newForm = { ...form, rent };
  await dispatch(processGrossAction(newForm));
};

export const onChangeChargesAction = (form, charges) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_CHARGES,
    payload: { newValue: charges }
  });
  const newForm = { ...form, charges };
  await dispatch(processGrossAction(newForm));
};

export const onChangeVacancyAction = (form, vacancy) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_VACANCY,
    payload: { newValue: vacancy }
  });
  const newForm = { ...form, vacancy };
  await dispatch(processGrossAction(newForm));
};

export const onChangeOtherIncomesAction = (form, otherIncomes) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_OTHER_INCOMES,
    payload: { newValue: otherIncomes }
  });
  const newForm = { ...form, otherIncomes };
  await dispatch(processGrossAction(newForm));
};

export const onChangePropertyTaxAction = (form, propertyTax) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_PROPERTY_TAX,
    payload: { newValue: propertyTax }
  });
  const newForm = { ...form, propertyTax };
  await dispatch(processNetAction(newForm));
};

export const onChangeCondominiumFeesAction = (form, condominiumFees) => async dispatch => {
  await dispatch({
    type: ACTION_TYPES.ON_CHANGE_CONDOMINIUM_FEES,
    payload: { newValue: condominiumFees }
  });
  const newForm = { ...form, condominiumFees };
  await dispatch(processNetAction(newForm));
};
