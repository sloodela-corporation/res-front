import React from 'react';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { Row, Col, UncontrolledTooltip, Input, Form, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import {
  onChangePriceAction,
  onChangeNotaryAction,
  onChangeAgencyAction,
  onChangeWorksAction,
  onChangeFurnituresAction,
  onChangeOtherExpensesAction,
  onChangeContributionAction,
  onChangeRentAction,
  onChangeChargesAction,
  onChangeVacancyAction,
  onChangeOtherIncomesAction,
  onChangePropertyTaxAction,
  onChangeCondominiumFeesAction,
  processAutomaticPriceNotary,
  processAutomaticPriceAgency
} from './seller.action';

interface ISellerFormProp extends DispatchProps, StateProps {}

class SellerForm extends React.Component<ISellerFormProp> {
  constructor(props) {
    super(props);
    this.handleOnChangePrice = this.handleOnChangePrice.bind(this);
    this.handleOnChangeNotary = this.handleOnChangeNotary.bind(this);
    this.handleOnChangeAgency = this.handleOnChangeAgency.bind(this);
    this.handleOnChangeWorks = this.handleOnChangeWorks.bind(this);
    this.handleOnChangeFurnitures = this.handleOnChangeFurnitures.bind(this);
    this.handleOnChangeOtherExpenses = this.handleOnChangeOtherExpenses.bind(this);
    this.handleOnChangeContribution = this.handleOnChangeContribution.bind(this);
    this.handleOnChangeRent = this.handleOnChangeRent.bind(this);
    this.handleOnChangeCharges = this.handleOnChangeCharges.bind(this);
    this.handleOnChangeVacancy = this.handleOnChangeVacancy.bind(this);
    this.handleOnChangeOtherIncomes = this.handleOnChangeOtherIncomes.bind(this);
    this.handleOnChangePropertyTax = this.handleOnChangePropertyTax.bind(this);
    this.handleOnChangePropertyTax = this.handleOnChangePropertyTax.bind(this);
    this.handleOnChangeIsNotary = this.handleOnChangeIsNotary.bind(this);
    this.handleOnChangeIsAgency = this.handleOnChangeIsAgency.bind(this);
  }

  handleOnChangePrice = e => {
    if (e.target.value) {
      this.props.onChangePriceAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangePriceAction(this.props.form, 0);
    }
  };

  handleOnChangeNotary = e => {
    if (e.target.value) {
      this.props.onChangeNotaryAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeNotaryAction(this.props.form, 0);
    }
  };

  handleOnChangeAgency = e => {
    if (e.target.value) {
      this.props.onChangeAgencyAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeAgencyAction(this.props.form, 0);
    }
  };

  handleOnChangeWorks = e => {
    if (e.target.value) {
      this.props.onChangeWorksAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeWorksAction(this.props.form, 0);
    }
  };

  handleOnChangeFurnitures = e => {
    if (e.target.value) {
      this.props.onChangeFurnituresAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeFurnituresAction(this.props.form, 0);
    }
  };

  handleOnChangeOtherExpenses = e => {
    if (e.target.value) {
      this.props.onChangeOtherExpensesAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeOtherExpensesAction(this.props.form, 0);
    }
  };

  handleOnChangeContribution = e => {
    if (e.target.value) {
      this.props.onChangeContributionAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeContributionAction(this.props.form, 0);
    }
  };

  handleOnChangeRent = e => {
    if (e.target.value) {
      this.props.onChangeRentAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeRentAction(this.props.form, 0);
    }
  };

  handleOnChangeCharges = e => {
    if (e.target.value) {
      this.props.onChangeChargesAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeChargesAction(this.props.form, 0);
    }
  };

  handleOnChangeVacancy = e => {
    if (e.target.value) {
      this.props.onChangeVacancyAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeVacancyAction(this.props.form, 0);
    }
  };

  handleOnChangeOtherIncomes = e => {
    if (e.target.value) {
      this.props.onChangeOtherIncomesAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeOtherIncomesAction(this.props.form, 0);
    }
  };

  handleOnChangePropertyTax = e => {
    if (e.target.value) {
      this.props.onChangePropertyTaxAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangePropertyTaxAction(this.props.form, 0);
    }
  };

  handleOnChangeCondominiumFees = e => {
    if (e.target.value) {
      this.props.onChangeCondominiumFeesAction(this.props.form, e.target.valueAsNumber);
    } else {
      this.props.onChangeCondominiumFeesAction(this.props.form, 0);
    }
  };

  handleOnChangeIsNotary = e => {
    this.props.processAutomaticPriceNotary(this.props.settings.notaryRate, this.props.form, e.target.checked);
  };

  handleOnChangeIsAgency = e => {
    this.props.processAutomaticPriceAgency(this.props.settings.agencyRate, this.props.form, e.target.checked);
  };

  render = () => (
    <Form>
      <Row>
        <Col xs="6">
          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.seller.form.legend.expenses" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.price.placeholder')}
                onChange={this.handleOnChangePrice}
                value={this.props.form.price || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-price">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-price">
                  <Translate contentKey="lmnp.old.seller.form.price.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <Input addon onChange={this.handleOnChangeIsNotary} type="checkbox" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder={translate('lmnp.old.seller.form.notary.placeholder')}
                onChange={this.handleOnChangeNotary}
                value={this.props.form.notary || ''}
                disabled={this.props.form.isNotaryAutomaticCalculation}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-notary">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-notary">
                  <Translate contentKey="lmnp.old.seller.form.notary.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <Input addon onChange={this.handleOnChangeIsAgency} type="checkbox" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                placeholder={translate('lmnp.old.seller.form.agency.placeholder')}
                onChange={this.handleOnChangeAgency}
                value={this.props.form.agency || ''}
                disabled={this.props.form.isAgencyAutomaticCalculation}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-agency">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-agency">
                  <Translate contentKey="lmnp.old.seller.form.agency.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.works.placeholder')}
                onChange={this.handleOnChangeWorks}
                value={this.props.form.works || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-works">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-works">
                  <Translate contentKey="lmnp.old.seller.form.works.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.furniture.placeholder')}
                onChange={this.handleOnChangeFurnitures}
                value={this.props.form.furnitures || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-furniture">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-furniture">
                  <Translate contentKey="lmnp.old.seller.form.furniture.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.other.expenses.placeholder')}
                onChange={this.handleOnChangeOtherExpenses}
                value={this.props.form.otherExpenses || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-other-expenses">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-other-expenses">
                  <Translate contentKey="lmnp.old.seller.form.other.expenses.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.contribution.placeholder')}
                onChange={this.handleOnChangeContribution}
                value={this.props.form.contribution || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-contribution">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-contribution">
                  <Translate contentKey="lmnp.old.seller.form.contribution.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>
        </Col>
        <Col xs="6">
          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.seller.form.legend.incomes" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.rent.placeholder')}
                onChange={this.handleOnChangeRent}
                value={this.props.form.rent || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-rent">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-rent">
                  <Translate contentKey="lmnp.old.seller.form.rent.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.charges.placeholder')}
                onChange={this.handleOnChangeCharges}
                value={this.props.form.charges || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-charges">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-charges">
                  <Translate contentKey="lmnp.old.seller.form.charges.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.vacancy.placeholder')}
                onChange={this.handleOnChangeVacancy}
                value={this.props.form.vacancy || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-vacancy">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-vacancy">
                  <Translate contentKey="lmnp.old.seller.form.vacancy.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.other.incomes.placeholder')}
                onChange={this.handleOnChangeOtherIncomes}
                value={this.props.form.otherIncomes || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-other-incomes">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-other-incomes">
                  <Translate contentKey="lmnp.old.seller.form.other.incomes.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>

          <fieldset>
            <legend>
              <small>
                <Translate contentKey="lmnp.old.seller.form.legend.fees" />
              </small>
            </legend>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.propertytax.placeholder')}
                onChange={this.handleOnChangePropertyTax}
                value={this.props.form.propertyTax || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-property-tax">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-property-tax">
                  <Translate contentKey="lmnp.old.seller.form.propertytax.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>

            <InputGroup size="sm">
              <Input
                placeholder={translate('lmnp.old.seller.form.condominiumfees.placeholder')}
                onChange={this.handleOnChangeCondominiumFees}
                value={this.props.form.condominiumFees || ''}
                type="number"
              />
              <InputGroupAddon addonType="append">
                <InputGroupText id="tooltip-condominium-fees">
                  <FontAwesomeIcon icon={faInfoCircle} />
                </InputGroupText>
                <UncontrolledTooltip placement="right" target="tooltip-condominium-fees">
                  <Translate contentKey="lmnp.old.seller.form.condominiumfees.tooltip" />
                </UncontrolledTooltip>
              </InputGroupAddon>
            </InputGroup>
          </fieldset>
        </Col>
      </Row>
    </Form>
  );
}

const mapStateToProps = (state: IRootState) => ({
  form: state.lmnpOldSeller,
  settings: state.lmnpOldSettings
});

const mapDispatchToProps = {
  onChangePriceAction,
  onChangeNotaryAction,
  onChangeAgencyAction,
  onChangeWorksAction,
  onChangeFurnituresAction,
  onChangeOtherExpensesAction,
  onChangeContributionAction,
  onChangeRentAction,
  onChangeChargesAction,
  onChangeVacancyAction,
  onChangeOtherIncomesAction,
  onChangePropertyTaxAction,
  onChangeCondominiumFeesAction,
  processAutomaticPriceNotary,
  processAutomaticPriceAgency
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SellerForm);
