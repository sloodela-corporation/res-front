export const processCalculation = (price = 0, percent = 0, isNeedToProceed = false) => {
  let result = 0;
  if (isNeedToProceed) {
    result = percent * price;
  }
  return Math.round(result * 100) / 100;
};
