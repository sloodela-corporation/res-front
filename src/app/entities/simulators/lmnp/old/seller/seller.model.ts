export interface ISellerModel {
  price: number;
  notary: number;
  agency: number;
  works: number;
  furnitures: number;
  otherExpenses: number;
  contribution: number;
  rent: number;
  charges: number;
  vacancy: number;
  otherIncomes: number;
  propertyTax: number;
  condominiumFees: number;
  isNotaryAutomaticCalculation: boolean;
  isAgencyAutomaticCalculation: boolean;
}
