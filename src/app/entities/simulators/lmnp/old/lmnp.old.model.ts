export interface ILmnpModel {
  tab: ITabLmnpModel;
  gross: number;
  net: number;
}

interface ITabLmnpModel {
  active: string;
}
