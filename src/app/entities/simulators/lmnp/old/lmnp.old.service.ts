export const processGrossReturn = ({
  price,
  notary,
  agency,
  works,
  furnitures,
  otherExpenses,
  contribution,
  rent,
  charges,
  vacancy,
  otherIncomes
}) => {
  let result = 0;
  const expensesResult = price + notary + agency + works + furnitures + otherExpenses - contribution;
  if (expensesResult) {
    const incomesResult = (rent + charges) * (12 - vacancy) + otherIncomes;
    result = (incomesResult / expensesResult) * 100;
  }
  return Math.round(result * 100) / 100;
};

export const processNetReturn = () => 0;
