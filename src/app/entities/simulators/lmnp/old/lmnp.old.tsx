import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Row, Col, TabPane, TabContent, Nav, NavItem, NavLink } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { changeTabAction } from './lmnp.old.action';
import SellerForm from './seller/seller.form';
import BuyerForm from './buyer/buyer.form';

interface ILmnpAncienProp extends StateProps, DispatchProps, RouteComponentProps {}

class LmnpAncien extends React.Component<ILmnpAncienProp> {
  constructor(props) {
    super(props);
  }

  toggle(tab) {
    if (this.props.tab.active !== tab) {
      this.props.changeTabAction(tab);
    }
  }

  handleOnChangeTab1 = () => this.toggle('1');
  handleOnChangeTab2 = () => this.toggle('2');

  render = () => (
    <>
      <Nav tabs>
        <NavItem>
          <NavLink className={classnames({ active: this.props.tab.active === '1' })} onClick={this.handleOnChangeTab1}>
            <Translate contentKey="lmnp.old.seller.tab" />
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={classnames({ active: this.props.tab.active === '2' })} onClick={this.handleOnChangeTab2}>
            <Translate contentKey="lmnp.old.buyer.tab" />
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={this.props.tab.active}>
        <TabPane tabId="1">
          <SellerForm />
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <Col sm="12">
              <BuyerForm />
            </Col>
          </Row>
        </TabPane>
      </TabContent>
    </>
  );
}

const mapStateToProps = (state: IRootState) => ({
  tab: state.lmnpOld.tab
});

const mapDispatchToProps = {
  changeTabAction
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LmnpAncien);
