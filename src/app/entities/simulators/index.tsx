import React from 'react';
import { Switch } from 'react-router-dom';
import LmnpOld from 'app/entities/simulators/lmnp/old/lmnp.old';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      <ErrorBoundaryRoute path={`${match.url}/lmnp/old`} component={LmnpOld} />
      {/* prettier-ignore */}
      {/* jhipster-needle-add-route-path - JHipster will routes here */}
    </Switch>
  </div>
);

export default Routes;
