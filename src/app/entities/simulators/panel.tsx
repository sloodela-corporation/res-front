import React from 'react';
import { UncontrolledTooltip, Card, CardTitle, CardText } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

interface IPanelProp {
  title: string;
  color: string;
  value: number;
  icon: IconDefinition;
  symbol: string;
  tooltipText: string;
  tooltipId: string;
}

export class Panel extends React.Component<IPanelProp> {
  constructor(props) {
    super(props);
  }

  render = () => (
    <Card body inverse color={this.props.color}>
      <CardTitle className="text-center h6">
        <strong>{this.props.title}</strong>
        <span id={this.props.tooltipId}>
          {' '}
          <FontAwesomeIcon icon={faInfoCircle} />
        </span>
        <UncontrolledTooltip target={this.props.tooltipId}>{this.props.tooltipText}</UncontrolledTooltip>
      </CardTitle>
      <CardText>
        <span className="h5 clearfix">
          <FontAwesomeIcon className="float-left" icon={this.props.icon} size="1x" />
          <strong className="float-right">
            {this.props.value} {this.props.symbol}
          </strong>
        </span>
      </CardText>
    </Card>
  );
}
